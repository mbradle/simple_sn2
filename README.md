This is the simple_sn2 project.  To obtain the project, clone it by typing

**git clone https://bitbucket.org/mbradle/simple_sn2**

Change into the directory and ensure that wn_user is installed.  Type

**cd simple_sn2**

**git clone https://bitbucket.org/mbradle/wn_user ../wn_user**

Steps to use:

1. Compile the codes by typing

**./project_make**

2. Obtain a pre-supernova xml file.  An example is available as *work/example_data/s25_pre_xml*.  These instructions will use that directly.

3. Run the explosion code:

**./simple_sn2 --libnucnet_xml work/example_data/s25_pre.xml --output_xml expl.xml 1.2e51**

The result of the explosion code is left in the output file *expl.xml*, which will be input to the network code.  The required positional parameter (chosen here as 1.2e51) is the supernova explosion energy.

4. Run the nucleosynthesis on the output of the explosion code:

**./multi_zone_network --libnucnet_xml expl.xml --output_xml out.xml "[@label1 < 200]" ""**

The output will be left in the file *out.xml*.  The first positional parameter (*"[@label1 < 200]"*) is a zone XPath expression for the parser to select a set of zones in the pre-supernova file to follow with the explosion code.  The second positional parameter (*""* in this example) gives a directory to output single-zone output zone xml files.  In this case, since it is empty, the code will dump no zone xml files.  To run all zones and leave the single-zone zone xml files in a directory *runs*, type:

**mkdir runs**

**./multi_zone_network --libnucnet_xml expl.xml --output_xml out.xml "" runs**

5.  To explore other options, type either

**./simple_sn2 --help**

or

**./multi_zone_network --help**

6.  The directory *work* contains scripts to facilitate running the codes.
