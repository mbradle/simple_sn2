////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Norberto J. Davila and Bradley S. Meyer.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code to compute simple shocks through a stellar model.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#ifdef WN_USER
#include "master.h"
#else
#include "default/master.h"
#endif

#include <iostream>
#include <fstream>
#include <Libnucnet.h>

#include "math/one_d_root_finder.hpp"

#include "user/thermo.h"

//##############################################################################
// Defines.
//##############################################################################

#define S_E0  "e0"
#define S_E   "e"
#define S_P0  "p0"
#define S_P   "p"
#define S_U   "u"

#define  S_MASS_BELOW     "mass below"
#define  S_OUTER_RADIUS   "cell outer radius"
#define  S_SHOCK_ENERGY   "shock_energy"
#define  S_SHOCK_SPEED    "shock_speed"
#define  S_SHOCK_RHO      "shock rho"
#define  S_SHOCK_T9       "shock t9"
#define  S_SHOCK_TIME     "shock time"
#define  S_PHOTON         "photon"

//##############################################################################
// sortByMass().
//##############################################################################

struct sortByMass
{
  bool operator()( const nnt::Zone& lhs, const nnt::Zone& rhs )
  {
    return
      lhs.getProperty<double>( S_MASS_BELOW )
      <
      rhs.getProperty<double>( S_MASS_BELOW );
  }
};

//##############################################################################
// shock_calculator().
//##############################################################################

class shock_calculator
{

  public:
    shock_calculator(){}
    shock_calculator( double _E ){ E = _E; d_r = 0; d_time = 0; }

    static double t9Post( double d_t9, nnt::Zone& zone )
    {

       zone.updateProperty( nnt::s_T9, d_t9 );
      
       double u_post =
         user::compute_thermo_quantity(
           zone,
           nnt::s_INTERNAL_ENERGY_DENSITY,
           S_PHOTON
         );

       return zone.getProperty<double>( S_U ) - u_post;      
    
     }

    static double rhoPost( double rho, nnt::Zone& zone )
    {

      zone.updateProperty(
        nnt::s_RHO,
        rho
      );

      double e0 = zone.getProperty<double>( S_E0 );

      double p0 = zone.getProperty<double>( S_P0 );

      double rho0 = zone.getProperty<double>( nnt::s_RHO_0 );

      double h0 = e0 + ( p0 / rho0 );
    
      wn_user::math::one_d_root_finder<>
        my_t9_root( boost::bind( t9Post, _1, boost::ref( zone ) ) );

      zone.updateProperty(
        nnt::s_T9,
        my_t9_root( 5. * zone.getProperty<double>( nnt::s_T9_0 ) )
      );

      double e = 
        user::compute_thermo_quantity(
          zone,
          nnt::s_INTERNAL_ENERGY_DENSITY,
          S_PHOTON
        ) / rho;

      zone.updateProperty( S_E, e );
 
      double p =
        user::compute_thermo_quantity(
          zone,
          nnt::s_PRESSURE,
          S_PHOTON
        );
 
      zone.updateProperty( S_P, p );

      double h = e + ( p / rho );

      return
        ( h - h0 )
        -
        ( 1. / 2. ) * ( ( 1. / rho0 ) + ( 1. / rho ) ) * ( p - p0 );
    }

    double computeShockSpeed( nnt::Zone& zone )
    {
      return
        pow(
          (
            zone.getProperty<double>( S_P ) -
            zone.getProperty<double>( S_P0 )
          ) /
          (
            zone.getProperty<double>( nnt::s_RHO_0 ) -
            pow( zone.getProperty<double>( nnt::s_RHO_0 ), 2 ) /
            zone.getProperty<double>( nnt::s_RHO )
          ),
          1. / 2.
        );
    }

    void
    operator()( nnt::Zone& zone )
    {

      nnt::normalize_zone_abundances( zone );

      zone.updateProperty(
        nnt::s_T9_0,
        zone.getProperty<double>( nnt::s_T9 )
      );

      zone.updateProperty(
        nnt::s_RHO_0,
        zone.getProperty<double>( nnt::s_RHO )
      );

      zone.updateProperty(
        nnt::s_RADIUS,
        zone.getProperty<double>( S_OUTER_RADIUS )
      );

      zone.updateProperty(
        S_E0,
        user::compute_thermo_quantity(
          zone, nnt::s_INTERNAL_ENERGY_DENSITY, nnt::s_TOTAL
        ) / zone.getProperty<double>( nnt::s_RHO_0 )
      );

      zone.updateProperty(
        S_P0,
        user::compute_thermo_quantity(
          zone, nnt::s_PRESSURE, nnt::s_TOTAL
        )
      );

      zone.updateProperty(
        S_U,
        E /
          ( ( 4./3. ) * ( M_PI ) *
            pow(
              zone.getProperty<double>( S_OUTER_RADIUS ),
              3.
            )
          )
      );

      wn_user::math::one_d_root_finder<>
        my_rho_root( boost::bind( rhoPost, _1, boost::ref( zone ) ) );

      zone.updateProperty(
        nnt::s_RHO,
        my_rho_root( zone.getProperty<double>( nnt::s_RHO_0 ) )
      );

      zone.updateProperty(
        S_SHOCK_RHO,
        zone.getProperty<double>( nnt::s_RHO )
      );

      zone.updateProperty(
        S_SHOCK_T9,
        zone.getProperty<double>( nnt::s_T9 )
      );

      double v_s = computeShockSpeed( zone );

      zone.updateProperty( S_SHOCK_SPEED, v_s );

      d_time +=
        ( zone.getProperty<double>( S_OUTER_RADIUS ) - d_r ) / v_s;

      d_r = zone.getProperty<double>( S_OUTER_RADIUS );

      zone.updateProperty( S_SHOCK_TIME, d_time );

    }

  private:
    double E, d_r, d_time;

};

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] )
{

  std::string line;

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  my_input.addPositionalParameter<double>(
    S_SHOCK_ENERGY, 1, "shock energy (enter as option or positional parameter)",
    "1.e51"
  );

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the network and output.
  //============================================================================

  wn_user::libnucnet_data my_libnucnet_data( v_map );

  wn_user::outputter my_outputter( v_map );

  //============================================================================
  // Set the output.
  //============================================================================
  
  my_outputter.set( my_libnucnet_data.getNucnet() );

  //============================================================================
  // Get zones and sort.
  //============================================================================

  std::vector<nnt::Zone> zones = my_libnucnet_data.getVectorOfZones();

  std::sort( zones.begin(), zones.end(), sortByMass() );

  //============================================================================
  // Create calculator().
  //============================================================================

  shock_calculator
    my_calc(
      my_input.getPositionalParameter<double>( v_map, S_SHOCK_ENERGY )
    );

  //============================================================================
  // Loop on the data.
  //============================================================================

  BOOST_FOREACH( nnt::Zone& zone, zones )
  {

    my_calc( zone );

    my_outputter( zone );

  }

  //============================================================================
  // Output.
  //============================================================================

  my_outputter.write();

  return EXIT_SUCCESS;

}
