import sys
import wnutils.xml as wx

xml = wx.Xml(sys.argv[1])

nuclides = xml.get_nuclide_data(sys.argv[2])

reactions = xml.get_reaction_data()

other = ['gamma', 'electron', 'positron', 'neutrino_e', 'anti-neutrino_e',
         'neutrino_mu', 'anti-neutrino_mu', 'neutrino_tau',
         'anti-neutrino_tau']

new_xml = wx.New_Xml(xml_type='libnucnet_input')

new_xml.set_nuclide_data(nuclides)

new_reactions = {}

for reaction in reactions:
    b_valid = True
    for reactant in reactions[reaction].reactants:
        if reactant not in nuclides:
            if reactant not in other:
                b_valid = False
    for product in reactions[reaction].products:
        if product not in nuclides:
            if product not in other: 
                b_valid = False
    if b_valid:
        new_reactions[reaction] = reactions[reaction]

new_xml.set_reaction_data(new_reactions)

zones = xml.get_zone_data()

new_zones = {}

for zone in zones:
    props = zones[zone]['properties']
    x = {}
    for sp in zones[zone]['mass fractions']:
        if sp[0] in nuclides:
            x[sp] = zones[zone]['mass fractions'][sp]
    new_zones[zone] = {'properties': props, 'mass fractions': x}

new_xml.set_zone_data(new_zones)
new_xml.write(sys.argv[3])
