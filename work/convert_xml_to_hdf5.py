import os
import argparse
import wnutils.xml as wx
import wnutils.h5 as w5

parser = argparse.ArgumentParser(description='Convert simple_sn2 output to hdf5.')
parser.add_argument('directory', metavar='dir', type=str,
                    help='the directory containing the output')
parser.add_argument('presn_xml', metavar='presn_xml', type=str,
                    help='the pre-supernova xml')
parser.add_argument('full_xml', metavar='full_xml', type=str,
                    help='the full output xml')
parser.add_argument('out_h5', metavar='out_h5', type=str,
                    help='the output hdf5')
parser.add_argument('--zones', metavar='zones', type=str,
                    default='zones',
                    help='the subdirectory containing the zone data')

args = parser.parse_args()

xml = wx.Xml(args.directory + '/' + args.full_xml)
nucs = xml.get_nuclide_data()

h5 = w5.New_H5(args.directory + '/' + args.out_h5, nucs)

zones = xml.get_zone_data()
h5.add_group('Expl/Full', zones)

xml_pre = wx.Xml(args.directory + '/' + args.presn_xml)
zones = xml_pre.get_zone_data()
h5.add_group('PreSn/Full', zones)

for filename in os.listdir(args.directory + '/' + args.zones):
    f = os.path.join(args.directory + '/' + args.zones, filename)
    xml = wx.Xml(f)
    zones = xml.get_zone_data()
    group = 'Expl/Runs/' + filename.split('.')[0]
    print(group)
    h5.add_group(group, zones)

