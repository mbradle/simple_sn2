This is a work directory in which one can run a series of single_zone calculations with an OpenMP script.  Before running, ensure that you have made the codes in ../ and have installed wnutils:

**cd ..**

**./project_make**

**cd work**

**python3 -m pip install --upgrade --user wnutils**

Steps to use:

1. Create an input directory.  The preferred name is *input/*.

2. In *run.sh*, specify simple_sn2 code directory, input directory, and output base directory relative to this directory (or use an absolute path).  The preferred choice is the leave them as defined in the script.

3. Place a presn xml file and a response file in the input directory.  The presn xml file might be, e. g., s25a28a29_presn.xml.  Might include neutrino xml (e.g., nu.xml) or other extra reaction xml files here as well (and specify in the response file).  To get started with an example, do, with the default choices,

**cp example_data/s25_pre.xml input/**

**cp example_data/explode.rsp input/**

4. To execute with OpenMP, run the *run.sh* script.  An example execution is:

**./run.sh --pre_sn s25_pre --expl 1.2e51 --output_dir my_output --nuc_xpath "[z <= 84]" --zone_xpath "[@label1 < 220]" --single_dir my_output/runs**

The *pre_sn*, *expl*, and *output_dir* parameters to the script are required parameters.  Others are optional.  To get help and a usage statement for the script, type:

**./run.sh --help**

Output is left in the chosen output directory (in the example, *my_output*).  If *single_dir* is set, zone xml files will be left in that directory and xml files that include the network from the output file *out.xml* in the output directory and zone data from the corresponding zone xml will be left in the directory specified by the option *new_sub_dir* (default: *zones*).  If the option *write_hdf5* is set to *true*, an output [HDF5](https://www.hdfgroup.org) file (*out.h5*) file will also be written (the default is *false*).

5. The script *run.pbs* gives an example [Portable Batch System](https://en.wikipedia.org/wiki/Portable_Batch_System) script for running multiple single-zone codes in parallel.  Pass the input through variables. The script *submit_pbs.sh* runs the pbs script.  Type, for example:

**./submit_pbs.sh --pre_sn s25_pre --expl 1.2e51 --nuc_xpath "[z <= 84]" --zone_xpath "[@label1 < 220]"**

The default is to use 32 cpus and 64Gb on one node with a wall time of 24 hours.  Use the optional parameter *resource_str* to set the pbs resources.  For example, you could type:

**./submit_pbs.sh --pre_sn s25_pre --expl 1.2e51 --nuc_xpath "[z <= 84]" --zone_xpath "[@label1 < 220]" --resource_str "select=1:ncpus=8:mem=16gb:ngpus=1:gpu_model=v100,walltime=24:00:00"**

If you supply the option *--single_sub_dir*, the code will output single zone output xml files, labeled by their zone label, in the chosen subdirectory under the output directory.  For example, type:

**./submit_pbs.sh --pre_sn s25_pre --expl 1.2e51 --nuc_xpath "[z <= 84]" --single_sub_dir runs**

This will run all zones and save the single-zone output xml files in the subdirectory *runs* in the output directory, corresponding xml files that include the
network and zone data in the subdirectory *zones* in the output directory (change the subdirectory with the options *new_sub_dir*), along with the full output xml.

The option *sn_suffix* will add a suffix to the main output directory.  For example, the command

**./submit_pbs.sh --pre_sn s25_pre --expl 1.2e51 --nuc_xpath "[z <= 84]" --single_sub_dir runs --sn_suffix 2**

will run the previous computation but will leave the output in the directory *output/sn25_pre_2/1.2e51* instead of the default *output/sn25_pre/1.2e51*.  This is useful for distinct runs with the same input pre-supernova model.

The default response file is *explode.rsp* in the *input* directory.  To use a different response file, say *my_new_explode.rsp* (in the *input* directory), use the option *--explode_rsp* and type

**./submit_pbs.sh --pre_sn s25_pre --expl 1.2e51 --nuc_xpath "[z <= 84]" --single_sub_dir runs --sn_suffix 2 --explode_rsp my_new_explode.rsp**

