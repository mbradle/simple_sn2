#!/bin/bash

margs=2

function example {
    echo -e "example: $0 --pre_sn s25_pre --expl 1.2e51 --nuc_xpath \"[z <= 84]\" --zone_xpath \"[@label1 < 200]\""
}

function usage {
    echo -e "usage: $script MANDATORY [OPTION]\n"
}

function help {
  usage
    echo -e "MANDATORY:"
    echo -e "  --pre_sn  VAL  The presupeernova model"
    echo -e "  --expl  VAL  The explosion energy\n"
    echo -e "OPTION:"
    echo -e "  --sn_suffix  VAL A suffix on the pre_sn (default: none)"
    echo -e "  --nuc_xpath  VAL The nuclear xpath (default: all nuclides)"
    echo -e "  --zone_xpath VAL  The zone xpath (default: all zones)"
    echo -e "  --code_dir VAL  The code directory (default: ../)"
    echo -e "  --input_dir VAL  The input directory (default: input)"
    echo -e "  --explode_rsp VAL  The input response file (default: explode.rsp)"
    echo -e "  --output_base VAL  The output directory base (default: output)"
    echo -e "  --single_sub_dir VAL  The single zone output sub directory (default: none)"
    echo -e "  --new_sub_dir VAL  The sub directory for single-zone xml include files (default: zones, but only if single_sub_dir set)"
    echo -e "  --resource_str VAL  The resource string."
    echo -e "  -h,  --help             Prints this help\n"
  example
}

function margs_precheck {
	if [ $2 ] && [ $1 -lt $margs ]; then
		if [ $2 == "--help" ] || [ $2 == "-h" ]; then
			help
			exit
		else
	    	usage
			example
	    	exit 1 # error
		fi
	fi
}

function margs_check {
	if [ $# -lt $margs ]; then
	    usage
	  	example
	    exit 1 # error
	fi
}

margs_precheck $# $1

# Default optional parameters

sn_suffix=""
code_dir=..
input_dir=input
explode_rsp="explode.rsp"
output_base=output
write_hdf5=false
nuc_xpath=""
zone_xpath=""
single_sub_dir=""
new_sub_dir="zones"
resource_str="select=1:ncpus=32:mem=64gb:ngpus=1:gpu_model=v100,walltime=24:00:00"

# Args while-loop

while [ "$1" != "" ];
do
   case $1 in
   --pre_sn)  shift
              pre_sn=$1
              ;;
   --sn_suffix)  shift
              sn_suffix=$1
              ;;
   --expl )   shift
              expl=$1
              ;;
   --nuc_xpath  )  shift
              nuc_xpath=$1
              ;;
   --zone_xpath  )  shift
              zone_xpath=$1
              ;;
   --code_dir  )  shift
              code_dir=$1
              ;;
   --input_dir  )  shift
              input_dir=$1
              ;;
   --explode_rsp  )  shift
              explode_rsp=$1
              ;;
   --output_base  )  shift
              output_base=$1
              ;;
   --write_hdf5  )  shift
              write_hdf5=$1
              ;;
   --single_sub_dir  )  shift
              single_sub_dir=$1
              ;;
   --new_sub_dir  )  shift
              new_sub_dir=$1
              ;;
   --resource_str  )  shift
              resource_str=$1
              ;;
   -h   | --help )        help
                          exit
                          ;;
   *)                     
                          echo "$script: illegal option $1"
                          usage
						  example
						  exit 1 # error
                          ;;
    esac
    shift
done

# Mandatory parameter check

margs_check $pre_sn $expl

if [ ! -z ${sn_suffix} ]
then
    suffix="_"${sn_suffix}
else
    suffix=${suffix} 
fi

output_dir=${output_base}/${pre_sn}${suffix}/${expl}

if [ ! -z ${single_sub_dir} ]
then
    single_dir=${output_dir}/${single_sub_dir}
else
    single_dir=""
fi

qsub -v pre_sn=${pre_sn},expl=${expl},nuc_xpath="${nuc_xpath}",zone_xpath="${zone_xpath}",output_dir=${output_dir},single_dir="${single_dir}",explode_rsp=${explode_rsp},write_hdf5=${write_hdf5}  -l "${resource_str}" -o ${output_dir} run.pbs
