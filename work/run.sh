#!/bin/bash

margs=3

function example {
    echo -e "example: $0 --pre_sn s25_pre --expl 1.2e51 --output_dir output --nuc_xpath \"[z <= 84]\" --zone_xpath \"[@label1 < 200]\""
}

function usage {
    echo -e "usage: $0 MANDATORY [OPTION]\n"
}

function help {
  usage
    echo -e "MANDATORY:"
    echo -e "  --pre_sn  VAL  The presupernova model"
    echo -e "  --expl  VAL  The explosion energy\n"
    echo -e "  --output_dir VAL  The output directory"
    echo -e "OPTION:"
    echo -e "  --nuc_xpath  VAL The nuclear xpath (default: all nuclides)"
    echo -e "  --zone_xpath VAL  The zone xpath (default: all zones)"
    echo -e "  --code_dir VAL  The code directory (default: ../)"
    echo -e "  --input_dir VAL  The input directory (default: input)"
    echo -e "  --explode_rsp VAL Name of explode response file (default: "explode.rsp")"
    echo -e "  --write_hdf5 VAL Set to write hdf5 file or not (default: "false")"
    echo -e "  --single_dir VAL  The single zone output directory (default: "")"
    echo -e "  --new_sub_dir VAL The include xml subdirectory for single-zone data off the output directory (default: "zones", but only if single_dir set)"
    echo -e "  -h,  --help             Prints this help\n"
  example
}

function margs_precheck {
	if [ $2 ] && [ $1 -lt $margs ]; then
		if [ $2 == "--help" ] || [ $2 == "-h" ]; then
			help
			exit
		else
	    	usage
			example
	    	exit 1 # error
		fi
	fi
}

function margs_check {
	if [ $# -lt $margs ]; then
	    usage
	  	example
	    exit 1 # error
	fi
}

margs_precheck $# $1

# Default optional parameters

code_dir=..
input_dir=input
nuc_xpath=""
zone_xpath=""
explode_rsp="explode.rsp"
single_dir=""
new_sub_dir="zones"
write_hdf5=false

# Args while-loop

while [ "$1" != "" ];
do
   case $1 in
   --pre_sn)  shift
              pre_sn=$1
              ;;
   --expl )   shift
              expl=$1
              ;;
   --output_dir  )  shift
              output_dir=$1
              ;;
   --nuc_xpath  )  shift
              nuc_xpath=$1
              ;;
   --zone_xpath  )  shift
              zone_xpath=$1
              ;;
   --code_dir  )  shift
              code_dir=$1
              ;;
   --input_dir  )  shift
              input_dir=$1
              ;;
   --explode_rsp  )  shift
              explode_rsp=$1
              ;;
   --write_hdf5  )  shift
              write_hdf5=$1
              ;;
   --single_dir  )  shift
              single_dir=$1
              ;;
   --new_sub_dir  )  shift
              new_sub_dir=$1
              ;;
   -h   | --help )        help
                          exit
                          ;;
   *)                     
                          echo "$script: illegal option $1"
                          usage
						  example
						  exit 1 # error
                          ;;
    esac
    shift
done

# Mandatory paramter check

margs_check $pre_sn $expl ${output_dir}

# Set up directories

mkdir -p ${output_dir}

if [ ! -z "${single_dir}" ]; then mkdir -p ${single_dir}; fi

# Set up output files

xml_file=${output_dir}/${expl}.xml

# Run the explosion code and apply xpaths

${code_dir}/simple_sn2 --libnucnet_xml ${input_dir}/${pre_sn}.xml --output_xml ${xml_file} ${expl}

python3 apply_nuc_xpath.py ${xml_file}  "${nuc_xpath}" ${xml_file}

cp ${input_dir}/${pre_sn}.xml ${output_dir}/pre_sn.xml

cp ${input_dir}/${explode_rsp} ${output_dir}/explode.rsp

# Run code

  ${code_dir}/multi_zone_network @${output_dir}/explode.rsp --libnucnet_xml $xml_file --output_xml ${output_dir}/out.xml "${zone_xpath}" "${single_dir}"

# Create xml include files.

if [ ! -z ${single_dir} ]
then
    mkdir -p ${output_dir}/${new_sub_dir}
    python3 create_include_xml.py ${output_dir} "${single_dir##*/}" ${new_sub_dir}
fi

# Create output hdf5 file if desired

if [ "$write_hdf5" = true ] ; then
    python3 convert_xml_to_hdf5.py ${output_dir}  pre_sn.xml out.xml out.h5
fi

# Create output tarball

if [ ! -z ${single_dir} ]
then
    if [ "$write_hdf5" = true ] ; then
        tar cvzf ${output_dir}/out.tar.gz ${output_dir}/pre_sn.xml ${output_dir}/out.xml ${output_dir}/runs ${output_dir}/zones ${output_dir}/out.h5
    else
        tar cvzf ${output_dir}/out.tar.gz ${output_dir}/pre_sn.xml ${output_dir}/out.xml ${output_dir}/runs ${output_dir}/zones
    fi
fi
