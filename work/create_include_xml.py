import os
import argparse

parser = argparse.ArgumentParser(description='Create XInclude XML.')

parser.add_argument('output_dir', metavar='output_dir', type=str,
                    help='the output directory')
parser.add_argument('single_sub_dir', metavar='single_sub_dir', type=str,
                    help='the single zone data subdirectory')
parser.add_argument('new_sub_dir', metavar='new_sub_dir', type=str,
                    help='the target subdirectory for the new xml')
parser.add_argument('--full_file', metavar='full_file', type=str,
                    default='out.xml',
                    help='the full xml')
parser.add_argument('--new_file_name', metavar='new_file_name', type=str,
                    default='out_',
                    help='the new file prefix')


args = parser.parse_args()

full_file = args.full_file

old_dir = args.output_dir + '/' + args.single_sub_dir
new_dir = args.output_dir + '/' + args.new_sub_dir

for filename in os.listdir(old_dir):
    old_file = old_dir + filename
    new_file = new_dir + '/' + filename.split('.')[0] + '.xml'

    f = open(new_file, "w")

    f.write("<?xml version=\"1.0\" encoding=\"ISO-8859-1\"?>\n")

    f.write("<libnucnet_input xmlns:xi=\"http://www.w3.org/2001/XInclude\">\n")
    f.write("   <xi:include href=\"../" + args.full_file + "\" xpointer=\"xpointer(//nuclear_network)\" />\n")
    f.write("   <xi:include href=\"../" + args.single_sub_dir + '/' + filename + "\" xpointer=\"xpointer(//zone_data)\" />\n")
    f.write("</libnucnet_input>\n")
    f.close()


