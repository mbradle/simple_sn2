////////////////////////////////////////////////////////////////////////////////
// This file was originally written by Bradley S. Meyer and Norberto J. Davila.
//
// This is free software; you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
//! \file
//! \brief Code for running a multi-zone network calculation.
////////////////////////////////////////////////////////////////////////////////

//##############################################################################
// Includes.
//##############################################################################

#ifdef WN_USER
#include "master2.h"
#else
#include "default/master2.h"
#endif

#include <malloc.h>

//##############################################################################
// Defines.
//##############################################################################

#define  S_MASS_BELOW                   "mass below"
#define  S_MULTI_ZONE_XPATH             "multi_zone_xpath"
#define  S_SINGLE_ZONE_OUTPUT_DIRECTORY "single_output_dir"

//##############################################################################
// sortByMass().
//##############################################################################

struct sortByMass
{
  bool operator()( const nnt::Zone& lhs, const nnt::Zone& rhs )
  {
    return
      lhs.getProperty<double>( S_MASS_BELOW )
      <
      rhs.getProperty<double>( S_MASS_BELOW );
  }
};

//##############################################################################
// main().
//##############################################################################

int main( int argc, char * argv[] ) {

  wn_user::v_map_t v_map;

  //============================================================================
  // Get the input.
  //============================================================================

  wn_user::inputter my_input;

  my_input.addPositionalParameter<std::string>(
    S_MULTI_ZONE_XPATH,
    1,
     "XPath to select zones to evolve",
    "\".//@label1[. < \'200\']\""
  );

  my_input.addPositionalParameter<std::string>(
    S_SINGLE_ZONE_OUTPUT_DIRECTORY,
    1,
     "Directory for output of single-zone data",
    "\"\""
  );

  v_map = my_input.getInput( argc, argv );

  //============================================================================
  // Set the network and zone data.
  //============================================================================

  wn_user::libnucnet_data my_libnucnet_data( v_map );

  //============================================================================
  // Create evolver, hydro, time_adjuster, limiter, outputter, modifier,
  // properties_updater, nse_corrector, screener, step_printer, and registerer.
  //============================================================================

  wn_user::network_time my_network_time( v_map );

  wn_user::time_adjuster my_time_adjuster( v_map );

  wn_user::network_limiter my_limiter( v_map );

  wn_user::outputter my_output( v_map );

  wn_user::rate_modifier my_rate_modifier( v_map );

  wn_user::rate_registerer my_rate_registerer( v_map );

  wn_user::nse_corrector my_nse_corrector( v_map );

  wn_user::screener my_screener( v_map );

  //============================================================================
  // Create zones and zone to evolve.
  //============================================================================

  std::vector<nnt::Zone> zones = my_libnucnet_data.getVectorOfZones();

  std::sort( zones.begin(), zones.end(), sortByMass() );

  std::vector<nnt::Zone> evolve_zones =
    my_libnucnet_data.getZonesFromVectorByXPath(
      zones,
      my_input.getPositionalParameter<std::string>( v_map, S_MULTI_ZONE_XPATH )
    );

  std::sort( evolve_zones.begin(), evolve_zones.end(), sortByMass() );

  //============================================================================
  // Set the output.
  //============================================================================

  my_output.set( my_libnucnet_data.getNucnet() );
  
  //============================================================================
  // Rate data updater.
  //============================================================================

  my_rate_registerer( my_libnucnet_data.getNetwork() );

  //============================================================================
  // Set the hydro.
  //============================================================================

  wn_user::hydro my_hydro( v_map );

  my_hydro.initialize( zones );

  wn_user::properties_updater my_properties_updater( v_map );

  //============================================================================
  // Single_zone output.
  //============================================================================

  std::string s_single_dir =
    my_input.getPositionalParameter<std::string>(
      v_map, S_SINGLE_ZONE_OUTPUT_DIRECTORY
    );

  //============================================================================
  // Iterate zones.
  //============================================================================

  #pragma omp parallel for schedule( dynamic, 1 )
    for( size_t i = 0; i < evolve_zones.size(); i++ )
    {

      std::cout << "Starting zone " << 
        Libnucnet__Zone__getLabel( evolve_zones[i].getNucnetZone(), 1 ) <<
        std::endl;

      wn_user::outputter my_zone_outputter( v_map, true );
      my_zone_outputter.set( my_libnucnet_data.getNucnet() );

      wn_user::network_evolver my_evolver( v_map );

      wn_user::matrix_modifier my_matrix_modifier( v_map );

      my_network_time.initializeTimes( evolve_zones[i] );

      my_rate_registerer.setZoneDataUpdater( evolve_zones[i] );

      my_screener( evolve_zones[i] );

      my_nse_corrector( evolve_zones[i] );

      my_rate_modifier( evolve_zones[i] );

      my_limiter( evolve_zones[i] );

      my_properties_updater.initialize( evolve_zones[i] );

  //============================================================================
  // Evolve network while t < final t.
  //============================================================================

      while( my_network_time.isLessThanEndTime( evolve_zones[i] ) )
      {

        evolve_zones[i].updateProperty(
          nnt::s_TIME,
          evolve_zones[i].getProperty<double>( nnt::s_TIME ) +
            evolve_zones[i].getProperty<double>( nnt::s_DTIME )
        ); 

        double t, t9, rho;

        t = evolve_zones[i].getProperty<double>( nnt::s_TIME );

        boost::tie( t9, rho ) = my_hydro( evolve_zones[i], t );

        evolve_zones[i].updateProperty( nnt::s_T9, t9 );

        evolve_zones[i].updateProperty( nnt::s_RHO, rho );

        my_rate_modifier( evolve_zones[i] );

        my_matrix_modifier( evolve_zones[i] );

        my_evolver( evolve_zones[i] );

        my_properties_updater( evolve_zones[i] );

        if( !s_single_dir.empty() )
        {
          my_zone_outputter( evolve_zones[i] );
        }

        my_time_adjuster( evolve_zones[i] );

        my_hydro.adjustTimeStep( evolve_zones[i] );

        my_limiter( evolve_zones[i] );

        malloc_trim( 0 );

      }

     Libnucnet__Zone__clearNetViews( evolve_zones[i].getNucnetZone() );
     Libnucnet__Zone__clearRates( evolve_zones[i].getNucnetZone() );

     if( !s_single_dir.empty() )
     {
       //#pragma omp critical
       //{
         std::string s_zone_xml =
           s_single_dir + "/" +
           std::string(
             Libnucnet__Zone__getLabel( evolve_zones[i].getNucnetZone(), 1 )
           ) + ".xml";
         my_zone_outputter.write( s_zone_xml );
         malloc_trim( 0 );
       //} 
     }

     std::cout << "Ending zone " <<
       Libnucnet__Zone__getLabel( evolve_zones[i].getNucnetZone(), 1 ) <<
       std::endl;

  }

  //============================================================================
  // Output.
  //============================================================================

  my_output( evolve_zones );

  my_output.write();

  //============================================================================
  // Done.
  //============================================================================

  return EXIT_SUCCESS;

}
